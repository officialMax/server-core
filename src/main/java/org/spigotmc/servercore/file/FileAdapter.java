package org.spigotmc.servercore.file;

import org.bukkit.configuration.file.YamlConfiguration;
import org.hytopia.plotsystem.PlotSystem;

import java.io.File;
import java.io.IOException;

public class FileAdapter {

  private File file;
  public YamlConfiguration yamlConfiguration;

  public FileAdapter(String fileDirectory, String fileName) {
    File directory = new File(PlotSystem.getInstance().getDataFolder() + "/" + fileDirectory);
    directory.mkdir();

    this.file = new File(PlotSystem.getInstance().getDataFolder() + "/" + fileDirectory, fileName + ".yml");
    if (!this.file.exists()) {
      try {
        this.file.createNewFile();
      } catch (IOException ignored) {}
    }
    this.yamlConfiguration = YamlConfiguration.loadConfiguration(file);
  }

  public void load() {

  }

  public void save() {
    try {
      this.yamlConfiguration.save(file);
    } catch (IOException ignored) { }
  }
}
