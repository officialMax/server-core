package org.spigotmc.servercore.file.local;

import org.spigotmc.servercore.file.FileAdapter;

public class BanConfig extends FileAdapter {
  public BanConfig(String fileName) {
    super("ban", fileName);
  }

  // create this if player gets banned
  // delete this if player gets unbanned
  // load this if server is starting

}
